package com.example.urubu.urubu.database;

import java.util.Date;

/**
 * Created by Norton on 04/10/2015.
 */
public class Login {
    private String UserName;
    private String UserID;
    private String Token;
    private String TokenVal;

    public Login() {
        this.Token = "";
        this.TokenVal = "";
        this.UserName = "";
        this.UserID = "";
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getTokenVal() {
        return TokenVal;
    }

    public void setTokenVal(String tokenVal) {
        TokenVal = tokenVal;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }
}
