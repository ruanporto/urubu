package com.example.urubu.urubu.webservice;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.urubu.urubu.CreateUserActivity;
import com.example.urubu.urubu.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Norton on 02/10/2015.
 */
public class CreateUserWebDAO {

    private Context context;
    private CriarUsuarioTela tela;

    public CreateUserWebDAO(Context context, CriarUsuarioTela tela){
        this.context = context;
        this.tela = tela;
    }

    public void CreateUser(String PsName, String PsLastName, String PsUserName, String PsPassword, String PsEmail){
        try{
            String sUrl = Settings.URL + "users";

            JSONObject jsonParams = new JSONObject();
            jsonParams.put("name", PsName);
            jsonParams.put("lastname", PsLastName);
            jsonParams.put("username", PsUserName);
            jsonParams.put("password", PsPassword);
            jsonParams.put("about", "");
            jsonParams.put("email", PsEmail);

            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, sUrl, jsonParams, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response){
                        try {
                            String sReturn = response.getString("return");
                            if(sReturn.equalsIgnoreCase("USER_CREATED")){
                                String sCreateUserMsg = context.getResources().getString(R.string.createuserok);
                                Toast.makeText(context, sCreateUserMsg, Toast.LENGTH_SHORT).show();
                                tela.finalizaTela();
                            } else {
                                String sError = "";
                                if(sReturn.equalsIgnoreCase("EMAIL_FAIL")){
                                    sError = context.getResources().getString(R.string.email_fail);
                                    Toast.makeText(context, sError, Toast.LENGTH_SHORT).show();
                                }

                                if(sReturn.equalsIgnoreCase("UNAME_FAIL")){
                                    sError = context.getResources().getString(R.string.uname_fail);
                                    Toast.makeText(context, sError, Toast.LENGTH_SHORT).show();
                                }

                                if(sReturn.equalsIgnoreCase("INVALID_PASSWORD")){
                                    sError = context.getResources().getString(R.string.invalid_password);
                                    Toast.makeText(context, sError, Toast.LENGTH_SHORT).show();
                                }

                                if(sReturn.equalsIgnoreCase("USER_NOT_CREATED")){
                                    sError = context.getResources().getString(R.string.user_not_created);
                                    Toast.makeText(context, sError, Toast.LENGTH_SHORT).show();
                                }

                                if(sReturn.equalsIgnoreCase("ERROR")){
                                    sError = context.getResources().getString(R.string.error);
                                    Toast.makeText(context, sError, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            throw new Exception(error.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

            RequestQueue queue = Volley.newRequestQueue(context);
            queue.add(jsObjRequest);
        }catch (Exception ex){
            Log.e("UsersWebDAO", ex.getMessage());
        }
    }
}
