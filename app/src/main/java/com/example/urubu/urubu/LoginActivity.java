package com.example.urubu.urubu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.EditText;

import com.example.urubu.urubu.database.Login;
import com.example.urubu.urubu.database.LoginDAO;
import com.example.urubu.urubu.webservice.LoginTela;
import com.example.urubu.urubu.webservice.LoginWebDAO;

public class LoginActivity extends ActionBarActivity implements LoginTela {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    @Override
    public void login(View v) {
        EditText userName = (EditText)findViewById(R.id.edt_username);
        EditText userPass = (EditText)findViewById(R.id.edt_password);
        String sUserName = userName.getText().toString();
        String sUserPass = userPass.getText().toString();

        LoginWebDAO oWebDao = new LoginWebDAO(this.getApplicationContext(), this);
        oWebDao.Login(sUserName, sUserPass);
    }

    @Override
    public void efetuaLogin(String PsToken, String PsTokenVal, String PsUserName, String PsUserID) {
        LoginDAO oLoginDAO = new LoginDAO(getApplicationContext());

        Login oLogin = new Login();
        oLogin.setToken(PsToken);
        oLogin.setTokenVal(PsTokenVal);
        oLogin.setUserName(PsUserName);
        oLogin.setUserID(PsUserID);
        oLoginDAO.insertToken(oLogin);

        //startActivity(new Intent(this, MainActivity.class));
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void criarUsuario(View v){
        Intent intent = new Intent(this, CreateUserActivity.class);
        startActivity(intent);
    }
}
