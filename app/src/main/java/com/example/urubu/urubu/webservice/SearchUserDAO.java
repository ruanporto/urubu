package com.example.urubu.urubu.webservice;

import android.content.Context;
import com.example.urubu.urubu.webservice.Message;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.urubu.urubu.database.Login;
import com.example.urubu.urubu.database.LoginDAO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ruan on 29/10/2015.
 */
public class SearchUserDAO {

    private Context context;
    //private SearchResultActivity telaSearch;
    private SearchUser tela;

    //CONSTRUCTOR
    public SearchUserDAO(SearchUser tela){
        this.context = (Context) tela;
        this.tela = tela;
    }

    //Follow User
    public void followUser(String userid_f){
        try {
            String sUrl = Settings.URL_usersSearch + "follow";
            //Obtem usuario logado
            LoginDAO oLoginDAO = new LoginDAO(context);
            Login oLogin = oLoginDAO.getLogin();
            Long tsLong = System.currentTimeMillis() / 1000;
            String ts = tsLong.toString();
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("userid", oLogin.getUserID().toString());
            jsonParams.put("token", oLogin.getToken().toString());
            jsonParams.put("userid_f", userid_f);

            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (Request.Method.POST, sUrl, jsonParams, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response){
                            try {
                                String sReturn = response.getString("return");

                                switch(sReturn){
                                    case "OK":
                                        tela.changeFollowButton();
                                        break;
                                    case "USER_NOT_FOUND":
                                        Toast.makeText(context, "User not found", Toast.LENGTH_SHORT).show();
                                        break;
                                    case "ERROR":
                                        Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                                        break;
                                    case "NOT_LOGGED":
                                        Toast.makeText(context, "User not logged", Toast.LENGTH_SHORT).show();
                                        break;
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            try {

                                throw new Exception(error.toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

            RequestQueue queue = Volley.newRequestQueue(context);
            queue.add(jsObjRequest);
        }
        catch(JSONException e){
            e.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    //Find the specific user related to the username used as parameter
    public void findUserByName(String username) {

        String url = Settings.URL_usersSearch + username;
        List<Message> messages = new ArrayList<Message>();

        JsonObjectRequest req = new JsonObjectRequest(
                Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response){

                List<User> user = new ArrayList<User>();

                //Get the ArrayJson from ObjectJson
                JSONArray jsonArray = null;
                try {
                    jsonArray = response.getJSONArray("return");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    for(int i =0; i < jsonArray.length(); i++) {
                        JSONObject jsonKeyValue = jsonArray.getJSONObject(i);
                        User usr = jsonbjToUser(jsonKeyValue);
                        user.add(usr);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Log.d("WBS", users.toString());
                tela.createUserHeader(user);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub
                Toast.makeText(context, "Problema ao buscar dados da web", Toast.LENGTH_SHORT).show();
                Log.d("WBS", error.toString());
            }
        }
        );

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(req);

    }

    //Get all the users with a similar name types in the search
    //<param name='name'>Name of the user to look for</param>
    public void findUsersByName(String name) {

        String url = Settings.URL_usersSearch + name;
        List<Message> messages = new ArrayList<Message>();

        JsonObjectRequest req = new JsonObjectRequest(
                Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response){

                List<User> users = new ArrayList<User>();

                //Get the ArrayJson from ObjectJson
                JSONArray jsonArray = null;
                try {
                    jsonArray = response.getJSONArray("return");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    for(int i =0; i < jsonArray.length(); i++) {
                        JSONObject jsonKeyValue = jsonArray.getJSONObject(i);
                        User usr = jsonbjToUser(jsonKeyValue);
                        users.add(usr);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Log.d("WBS", users.toString());
                tela.populateUsersListOnScreen(users);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub
                Toast.makeText(context, "Problema ao buscar dados da web", Toast.LENGTH_SHORT).show();
                Log.d("WBS", error.toString());
            }
        }
        );

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(req);

    }

    //Find messages related to hashtags
    public void findMsgsWithHashtag(String hashtag){
        String url = Settings.URL_hashtagMessages + hashtag;
        List<Message> messages = new ArrayList<Message>();

        JsonObjectRequest req = new JsonObjectRequest(
                Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response){

                List<Message> messages = new ArrayList<Message>();

                //Get the ArrayJson from ObjectJson
                JSONArray jsonArray = null;
                try {
                    jsonArray = response.getJSONArray("return");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    for(int i =0; i < jsonArray.length(); i++) {
                        JSONObject jsonKeyValue = jsonArray.getJSONObject(i);
                        Message msg = jsonbjToMessage(jsonKeyValue);
                        messages.add(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Log.d("WBS", users.toString());
                tela.populateHashtagMessagesOnScreen(messages);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub
                Toast.makeText(context, "Problema ao buscar dados da web", Toast.LENGTH_SHORT).show();
                Log.d("WBS", error.toString());
            }
        }
        );

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(req);
    }


    //Tranform json object into a specific class obj based on the atribute's name declared on the API doc
    private User jsonbjToUser(JSONObject json){
        User usr = new User();
        try{
            usr.set_id(json.getString("id"));
            usr.set_name(json.getString("name"));
            usr.set_lastName(json.getString("lastname"));
            usr.set_userName(json.getString("username"));
            usr.set_email(json.getString("email"));
            usr.set_avatar(json.getString("avatar"));

        }
        catch (JSONException e){
            e.printStackTrace();
        }

        return usr;
    }

    //Tranform json object into a specific class obj based on the atribute's name declared on the API doc
    private Message jsonbjToMessage(JSONObject json){
        Message msg = new Message();
        try{
            msg.setId(json.getInt("id"));
            msg.setText(json.getString("text"));
            msg.setUser(json.getString("user"));
            msg.setRtcount(json.getInt("rtcount"));
            msg.setFavcount(json.getInt("favcount"));
            msg.setDatetime(json.getString("datetime"));
            msg.setImage(json.getString("image").equalsIgnoreCase("null") ? "" : json.getString("image"));
            msg.setAvatar(json.getString("avatar").equalsIgnoreCase("null") ? "" : json.getString("avatar"));

            String sLoc = json.getString("location");
            if(sLoc.equalsIgnoreCase("null")){
                msg.setLocation(null);
            } else {
                JSONObject oJSOLoc = json.getJSONObject("location");
                Location oLoc = new Location();
                oLoc.setLat(oJSOLoc.getString("lat"));
                oLoc.setLng(oJSOLoc.getString("lng"));
                msg.setLocation(oLoc);
            }
        }
        catch (JSONException e){
            e.printStackTrace();
        }

        return msg;
    }
}