package com.example.urubu.urubu.webservice;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.urubu.urubu.R;
import com.example.urubu.urubu.database.Login;
import com.example.urubu.urubu.database.LoginDAO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Norton on 08/11/2015.
 */
public class NewFollowsWebDAO {

    private Context context;
    private MainTela tela;

    public NewFollowsWebDAO(Context context, MainTela tela){
        this.context = context;
        this.tela = tela;
    }

    public void getNewFriendInvites(){
        try{
            //Obtem usuario logado
            LoginDAO oLoginDAO = new LoginDAO(context);
            final Login oLogin = oLoginDAO.getLogin();

            //Obtem url
            String sUrl = Settings.URL + "users/news";

            //Prepara JSON
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("userid", oLogin.getUserID());
            jsonParams.put("token", oLogin.getToken());

            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (Request.Method.POST, sUrl, jsonParams, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response){
                            try {
                                String sReturn = response.getString("return");
                                if(!sReturn.equalsIgnoreCase("NOTFOUND") && !sReturn.equalsIgnoreCase("ERROR") && !sReturn.equalsIgnoreCase("NOT_LOGGED") ){
                                    JSONArray oJsArray = response.getJSONArray("return");
                                    if(oJsArray.length() > 0)
                                        tela.showNewFriendsNotify();
                                } else {
                                    String sError = "";
                                    if(sReturn.equalsIgnoreCase("NOTFOUND")){
                                        sError = context.getResources().getString(R.string.notfound);
                                        Toast.makeText(context, sError, Toast.LENGTH_SHORT).show();
                                    }

                                    if(sReturn.equalsIgnoreCase("ERROR")){
                                        sError = context.getResources().getString(R.string.error);
                                        Toast.makeText(context, sError, Toast.LENGTH_SHORT).show();
                                    }

                                    if(sReturn.equalsIgnoreCase("NOT_LOGGED")){
                                        sError = context.getResources().getString(R.string.not_logged);
                                        Toast.makeText(context, sError, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            try {
                                throw new Exception(error.toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

            RequestQueue queue = Volley.newRequestQueue(context);
            queue.add(jsObjRequest);

        } catch(Exception ex){
            ex.printStackTrace();
        }
    }

}
