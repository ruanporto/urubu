package com.example.urubu.urubu.webservice;

import android.app.DownloadManager;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.urubu.urubu.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Norton on 02/10/2015.
 */
public class LoginWebDAO {

    private Context context;
    private LoginTela tela;

    public LoginWebDAO(Context context, LoginTela tela){
        this.context = context;
        this.tela = tela;
    }

    public void Login(String PsUserName, String PsPassword){
        try{
            String sUrl = Settings.URL + "login";

            JSONObject jsonParams = new JSONObject();
            jsonParams.put("username", PsUserName);
            jsonParams.put("password", PsPassword);

            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, sUrl, jsonParams, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response){
                        try {
                            String sReturn = response.getString("return");
                            if(!sReturn.equalsIgnoreCase("FALSE") && !sReturn.equalsIgnoreCase("ERROR")){
                                JSONObject sObject = response.getJSONObject("return");
                                String token = sObject.getString("token");
                                String tokenval = sObject.getString("tokenval");
                                String username = sObject.getString("username");
                                String userid = sObject.getString("id");
                                tela.efetuaLogin(token, tokenval, username, userid);
                            } else {
                                String sError = context.getResources().getString(R.string.login_error);
                                Toast.makeText(context, sError, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            throw new Exception(error.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

            RequestQueue queue = Volley.newRequestQueue(context);
            queue.add(jsObjRequest);
        }catch (Exception ex){
            Log.e("UsersWebDAO", ex.getMessage());
        }
    }
}
