package com.example.urubu.urubu.telasconteudo.telasexternas;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.example.urubu.urubu.LoadImage;
import com.example.urubu.urubu.R;
import com.example.urubu.urubu.webservice.Message;
import com.example.urubu.urubu.webservice.MessageDAO;
import com.example.urubu.urubu.webservice.SearchUser;
import com.example.urubu.urubu.webservice.SearchUserDAO;
import com.example.urubu.urubu.webservice.Settings;
import com.example.urubu.urubu.webservice.User;

import java.util.List;

public class UserMessages extends AppCompatActivity implements SearchUser{
    public List<Message> listMessages;
    private String userid_f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_messages);

        String name = "";
        String username = "";
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            //Set the screen title to user' name
            name = bundle.getString(SearchResultActivity.EXTRA_NAME);
            getSupportActionBar().setTitle(name);

            //get username to look for the user' messages
            username = bundle.getString(SearchResultActivity.EXTRA_USERNAME);
        }

        //First of all, it must create the user HEADER so it will find the user and create header
        new SearchUserDAO(this).findUserByName(username);

        //Calling method to find the user' messages by using Urubu API with webservice
        new MessageDAO(this).getAllMessages(username);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.menu_messages, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    //Set an adpter to the list of messages and link to ListView showing on screen
    public void populateMsgsOnScreen(List<Message> messages){

        UserMessagesAdapter msgAdapter = new UserMessagesAdapter(this, messages);
        ListView listView = (ListView) findViewById(R.id.messagesList);

        listView.setAdapter(msgAdapter);
    }

    @Override
    public void populateUsersListOnScreen(List<User> userList) {
        //NOT USED
    }

    @Override
    public void populateHashtagMessagesOnScreen(List<Message> messagesList) {
        //NOT USED
    }

    //It creates the header of the user' messages, it includes user picture, name, email, and other infos
    public void createUserHeader(List<User> userList){

        //como procurou por um usuario especifico entao so tem um registro na lista
        User user = userList.get(0);
        this.userid_f = user.get_id(); //armazena o id do usuario para atrelar ao botao de follow

        //Screen components
        ImageView usrPic = (ImageView) findViewById(R.id.user_picture);
        TextView name = (TextView) findViewById(R.id.user_name);
        TextView email = (TextView) findViewById(R.id.user_email);
        TextView about = (TextView) findViewById(R.id.user_about);

        //seta valores nos componentes de tela

        //Verifica se existe imagem no post, faz o download e apresenta
        if(user.get_avatar().length() > 0){
            String sImgAva = Settings.URL_uploads + user.get_avatar();
            ImageLoader avaLoad = LoadImage.getInstance(this).getImageLoader();
            avaLoad.get(sImgAva, avaLoad.getImageListener(usrPic,
                            R.drawable.ic_account_circle_black_48dp,
                            R.drawable.ic_account_circle_black_48dp),
                    600,
                    600
            );
        }

        name.setText(user.get_name());
        email.setText(user.get_email());
        about.setText(user.get_about());
    }

    //Action of follow User button
    public void followUser(View view){
        if(!userid_f.equals("")) {
            new SearchUserDAO(this).followUser(this.userid_f);
        }
    }

    //If it was possible to follow a user then it must change the follow button layout
    public void changeFollowButton(){
        Button followButton = (Button) findViewById(R.id.id_button_follow);
        followButton.setBackgroundColor(Color.parseColor("#fffff"));
    }
}
