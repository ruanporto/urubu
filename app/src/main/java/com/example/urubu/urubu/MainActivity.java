package com.example.urubu.urubu;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.urubu.urubu.database.Login;
import com.example.urubu.urubu.database.LoginDAO;
import com.example.urubu.urubu.telasconteudo.telasexternas.NewMessage;
import com.example.urubu.urubu.telasconteudo.telasinternas.TimelineFragment;
import com.example.urubu.urubu.webservice.MainTela;
import com.example.urubu.urubu.webservice.Message;
import com.example.urubu.urubu.webservice.NewFollowsWebDAO;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, TimelineFragment.OnMessageSelectedListener,
        MainTela {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //check if the user is logged
        chkUserIsLogged();

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        //Verifica solicitações de novas amizades
        NewFollowsWebDAO oNFWD = new NewFollowsWebDAO(getApplicationContext(), this);
        oNFWD.getNewFriendInvites();
    }


    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_home);

                //Push the load of timeline messages
                //TODO - Fix the LOADING problem where it seems to be always loading something
                getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, new TimelineFragment())
                    .commit();

                break;
            case 2:
                mTitle = getString(R.string.title_trends);
                break;
            case 3:
                mTitle = getString(R.string.title_config);
                startActivity(new Intent(this, SettingsActivity.class));
                break;
            case 4:
                LoginDAO loginDAO = new LoginDAO(this);
                //Delete the person register from local database so the log out is done
                loginDAO.deletePessoa(loginDAO.getLogin());
                startActivity(new Intent(this, LoginActivity.class)); //Redirect to login screen
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            //restoreActionBar();

            // Associate searchable configuration with the SearchView
            SearchManager searchManager =  (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.newmessage) {
            Intent iNewMessage = new Intent(this, NewMessage.class);
            startActivity(iNewMessage);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onArticleSelected(Message PoMessage) {
        Intent iMDActivity = new Intent(this, MessageDetailActivity.class);
        Bundle bArgs = new Bundle();
        bArgs.putSerializable("MSG", PoMessage);
        iMDActivity.putExtras(bArgs);
        startActivity(iMDActivity);
    }

    @Override
    public void showNewFriendsNotify() {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(android.R.drawable.ic_dialog_alert)
                        .setContentTitle("Urubu - Novos Amigos")
                        .setContentText("Você possui novos amigos... Acesse já para aprová-los!");

/*        //Configurando Intent para abrir tela quando clicar na notificacao
        Intent resultIntent = new Intent(this, FirstActivity.class);

        // Because clicking the notification opens a new ("special") activity, there's
        // no need to create an artificial back stack.
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        //configurando a acao do clique na notificacao
        mBuilder.setContentIntent(resultPendingIntent);*/

        // Sets an ID for the notification
        int mNotificationId = 001;

        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            // return rootView;

            return null;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

    //Check if the user is logged, if not then call the log in screen
    public void chkUserIsLogged(){
        try {
            LoginDAO oLoginDao = new LoginDAO(this.getApplicationContext());
            if (oLoginDao.databaseExists()) {
                Login oLogin = oLoginDao.getLogin();
                if (oLogin != null) {
                    validateToken(oLogin);
                } else {
                    startActivity(new Intent(this, LoginActivity.class));
                }
            } else {
                startActivity(new Intent(this, LoginActivity.class));
            }
        }
        catch(ParseException e){
            e.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public void validateToken(Login PoLogin) throws ParseException {
        SimpleDateFormat oDtFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            Date dtTokenVal = oDtFormat.parse(PoLogin.getTokenVal());
            int res = dtTokenVal.compareTo(new Date());
            if (res == -1) {
                Toast.makeText(getApplicationContext(), "Seu login expirou!", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this, LoginActivity.class));
            }
        } catch (ParseException e) {
            throw e;
        }
    }



}
