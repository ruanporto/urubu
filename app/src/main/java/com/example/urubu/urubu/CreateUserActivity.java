package com.example.urubu.urubu;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.urubu.urubu.webservice.CreateUserWebDAO;
import com.example.urubu.urubu.webservice.CriarUsuarioTela;

public class CreateUserActivity extends ActionBarActivity implements CriarUsuarioTela {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);
    }

    public void validaLogin(View v){
        EditText edtName = (EditText)findViewById(R.id.edtName);
        EditText edtLastName = (EditText)findViewById(R.id.edtLastName);
        EditText edtUserName = (EditText)findViewById(R.id.edtUserName);
        EditText edtPassword = (EditText)findViewById(R.id.edtPassword);
        EditText edtEmail = (EditText)findViewById(R.id.edtEmail);

        String sName = edtName.getText().toString();
        String sLastName = edtLastName.getText().toString();
        String sUserName = edtUserName.getText().toString();
        String sPassword = edtPassword.getText().toString();
        String sEmail = edtEmail.getText().toString();

        boolean bValid = true;

        if(sName.length() == 0 && bValid){
            Toast.makeText(getApplicationContext(), "Informe o seu nome!", Toast.LENGTH_SHORT).show();
            bValid = false;
        }

        if(sLastName.length() == 0 && bValid){
            Toast.makeText(getApplicationContext(), "Informe o seu sobrenome!", Toast.LENGTH_SHORT).show();
            bValid = false;
        }

        if(sUserName.length() == 0 && bValid){
            Toast.makeText(getApplicationContext(), "Informe o seu nome de usuário!", Toast.LENGTH_SHORT).show();
            bValid = false;
        }

        if(sPassword.length() == 0 && bValid){
            Toast.makeText(getApplicationContext(), "Informe a sua senha!", Toast.LENGTH_SHORT).show();
            bValid = false;
        }

        if(sEmail.length() == 0 && bValid){
            Toast.makeText(getApplicationContext(), "Informe o seu e-mail!", Toast.LENGTH_SHORT).show();
            bValid = false;
        }

        if(!validaEmail(sEmail) && bValid){
            Toast.makeText(getApplicationContext(), "Informe em e-mail válido!", Toast.LENGTH_SHORT).show();
            bValid = false;
        }

        if(bValid){
            CreateUserWebDAO oCreateDAO = new CreateUserWebDAO(getApplicationContext(), this);
            oCreateDAO.CreateUser(sName, sLastName, sUserName, sPassword, sEmail);
        }
    }

    public boolean validaEmail(String PsEmail){
        if (Patterns.EMAIL_ADDRESS.matcher(PsEmail).matches())
            return true;
        else
            return false;
    }

    public void finalizaTela(){
        finish();
    }
}
