package com.example.urubu.urubu.telasconteudo.telasexternas;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.urubu.urubu.MessageDetailActivity;
import com.example.urubu.urubu.R;
import com.example.urubu.urubu.webservice.Message;
import com.example.urubu.urubu.webservice.SearchUser;
import com.example.urubu.urubu.webservice.SearchUserDAO;
import com.example.urubu.urubu.webservice.User;

import java.util.List;

public class SearchResultActivity extends AppCompatActivity implements SearchUser{

    public final static String EXTRA_NAME = "com.example.urubu.urubu.telasconteudo.telasexternas.SearchResultActivity.NAME";
    public final static String EXTRA_USERNAME = "com.example.urubu.urubu.telasconteudo.telasexternas.SearchResultActivity.USERNAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        //call method to handle the users/hashtag search
        search(getIntent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search_result, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    //Get the query from the search bar and call the webservice method to find the users if related names
    private void search(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);

            //Call the method in charge for retrieving the content from the webservice API
            SearchUserDAO searchUserDAO = new SearchUserDAO(this);

            //se o primeiro caractere da query da pesquisa for @ entao é uma pesquisa por HASHTAG
            if(query.substring(0, 1).equals("#")){
                searchUserDAO.findMsgsWithHashtag(query.substring(1, query.length()));
            }
            else{
                // ArrayList<User> listUsers = searchUserDAO.findUsersByName(query);
                searchUserDAO.findUsersByName(query);
            }
        }
    }

    //Fill the list on screen with the search' result
    public void populateUsersListOnScreen(List<User> usersList){
        SearchResultAdapter msgAdapter = new SearchResultAdapter(usersList);
        final ListView listView = (ListView) findViewById(R.id.list_pesq_result);

        //Set the onclick listener for each list item
        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                        User selectedUser = (User) listView.getItemAtPosition(position);

                        //Call the selected user messages screen according to its username
                        Intent intent = new Intent(getBaseContext(), UserMessages.class);
                        intent.putExtra(SearchResultActivity.EXTRA_NAME, selectedUser.get_name());
                        intent.putExtra(SearchResultActivity.EXTRA_USERNAME, selectedUser.get_userName());

                        startActivity(intent);
                    }
                });

        listView.setAdapter(msgAdapter);
    }

    public void populateHashtagMessagesOnScreen(List<Message> messagesList){
        SearchResultHashTagAdapter msgAdapter = new SearchResultHashTagAdapter(messagesList);
        final ListView listView = (ListView) findViewById(R.id.list_pesq_result);

        //Set the onclick listener for each list item
        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                        Message selectedMessage = (Message) listView.getItemAtPosition(position);

                        Intent iMDActivity = new Intent(getBaseContext(), MessageDetailActivity.class);
                        Bundle bArgs = new Bundle();
                        bArgs.putSerializable("MSG", selectedMessage);
                        iMDActivity.putExtras(bArgs);
                        startActivity(iMDActivity);
                    }
                });

        listView.setAdapter(msgAdapter);
    }

    @Override
    public void createUserHeader(List<User> user) {
        //NOT USED
    }

    @Override
    public void changeFollowButton() {
        //NOT USED
    }
}
