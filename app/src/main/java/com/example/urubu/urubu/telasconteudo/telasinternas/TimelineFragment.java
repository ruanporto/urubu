package com.example.urubu.urubu.telasconteudo.telasinternas;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import com.example.urubu.urubu.TimelineAdpater;
import com.example.urubu.urubu.webservice.Message;
import com.example.urubu.urubu.webservice.TimelineTela;
import com.example.urubu.urubu.webservice.TimelineWebDAO;

import java.util.List;

public class TimelineFragment extends android.support.v4.app.ListFragment implements TimelineTela {

    OnMessageSelectedListener mCallback;
    List<Message> lMsg = null;


    // The container Activity must implement this interface so the frag can deliver messages
    public interface OnMessageSelectedListener {
        /** Called by MensagensFragment when a list item is selected */
        public void onArticleSelected(Message PoMessage);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Obtem dados do webservice
        TimelineWebDAO oTimeline = new TimelineWebDAO(getActivity().getApplicationContext(), this);
        oTimeline.obterTimeline();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (OnMessageSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        // Notify the parent activity of selected item
        Message oMsg = lMsg.get(position);
        mCallback.onArticleSelected(oMsg);
    }

    @Override
    public void populateTimeline(List<Message> PsListMsg) {
        lMsg = PsListMsg;
        setListAdapter(new TimelineAdpater(PsListMsg));
    }
}
