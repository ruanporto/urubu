package com.example.urubu.urubu;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.example.urubu.urubu.webservice.Location;
import com.example.urubu.urubu.webservice.Message;
import com.example.urubu.urubu.webservice.Settings;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MessageDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_messages);

        //Obtem objeto enviado da outra acitivity
        Bundle b = getIntent().getExtras();
        Message oMsg = (Message)b.getSerializable("MSG");

        //Implementa a tela
        setScreenData(oMsg);
    }

    public void setScreenData(Message PoMsg){
        try{
            //Objetos para ajuste de data
            SimpleDateFormat oDtFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            SimpleDateFormat oDtShowF = new SimpleDateFormat("dd/MM/yyyy");

            //Objeto mensagem
            Message msg = PoMsg;

            //Componentes de tela
            ImageView profileImage = (ImageView)findViewById(R.id.imgProfile);
            TextView txtMessage = (TextView)findViewById(R.id.txtMessage);
            TextView txtUserName = (TextView)findViewById(R.id.txtUserName);
            TextView txtDate = (TextView)findViewById(R.id.txtMsgDt);
            ImageView imageMsg = (ImageView)findViewById(R.id.imgMessage);
            LinearLayout layoutLoc = (LinearLayout)findViewById(R.id.layoutLoc);
            TextView txtLoc = (TextView)findViewById(R.id.txtLoc);

            SpannableString ssMsg = new SpannableString(msg.getText());
            Matcher matcher = Pattern.compile("#[A-Za-z0-9_-]+\\b").matcher(msg.getText());
            while (matcher.find())
            {
                //componente para destacar/clicar em hashtags
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View textView) {
                        Toast.makeText(textView.getContext(), "Click", Toast.LENGTH_LONG).show();
                    }
                };
                ssMsg.setSpan(clickableSpan, matcher.start(), matcher.end(), 0);
            }
            //Adiciona texto da mensagem
            txtMessage.setText(ssMsg);
            txtMessage.setMovementMethod(LinkMovementMethod.getInstance());
            txtMessage.setClickable(false);

            //Adiciona nome do usuário
            txtUserName.setText(msg.getUser());

            //Adiciona data da mensagem
            Date oDateMsg = oDtFormat.parse(msg.getDatetime());
            txtDate.setText(oDtShowF.format(oDateMsg));

            //Verifica se existe imagem no post, faz o download e apresenta na timeline
            if(msg.getImage().length() > 0){
                String sImgMsg = Settings.URL_uploads + msg.getImage();
                ImageLoader imgLoad = LoadImage.getInstance(this).getImageLoader();
                imgLoad.get(sImgMsg, imgLoad.getImageListener(imageMsg,
                                R.drawable.ic_photo_size_select_actual_black_24dp,
                                R.drawable.ic_photo_size_select_actual_black_24dp),
                        600,
                        600
                );
                imageMsg.setVisibility(View.VISIBLE);
                imageMsg.setClickable(false);
            }

            //Verifica se existe imagem no post, faz o download e apresenta na timeline
            if(msg.getAvatar().length() > 0){
                String sImgAva = Settings.URL_uploads + msg.getAvatar();
                ImageLoader avaLoad = LoadImage.getInstance(this).getImageLoader();
                avaLoad.get(sImgAva, avaLoad.getImageListener(profileImage,
                                R.drawable.ic_account_circle_black_48dp,
                                R.drawable.ic_account_circle_black_48dp),
                        600,
                        600
                );
                profileImage.setClickable(false);
            }

            if(msg.getLocation() != null){
                Location oLoc = msg.getLocation();
                String sLatLng = oLoc.getLat() + "," + oLoc.getLng();

                //componente para abrir mapa
                ClickableSpan clickMap = new ClickableSpan() {
                    @Override
                    public void onClick(View textView) {
                        //Get contoller
                        TextView tv = (TextView) textView;
                        // Create a Uri from an intent string. Use the result to create an Intent.
                        Uri gmmIntentUri = Uri.parse("geo:" + tv.getText().toString());
                        // Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        // Make the Intent explicit by setting the Google Maps package
                        mapIntent.setPackage("com.google.android.apps.maps");
                        // Attempt to start an activity that can handle the Intent
                        textView.getContext().startActivity(mapIntent);
                    }
                };

                SpannableString ssMap = new SpannableString(sLatLng);
                ssMap.setSpan(clickMap, 0, sLatLng.length(), 0);
                txtLoc.setText(ssMap);

                txtLoc.setMovementMethod(LinkMovementMethod.getInstance());
                layoutLoc.setVisibility(View.VISIBLE);
            }
        } catch(Exception ex){
            ex.printStackTrace();
        }
    }

}
