package com.example.urubu.urubu.webservice;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.urubu.urubu.R;
import com.example.urubu.urubu.database.Login;
import com.example.urubu.urubu.database.LoginDAO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Norton on 31/10/2015.
 */
public class TimelineWebDAO {

    private Context context;
    private TimelineTela tela;

    public TimelineWebDAO(Context context, TimelineTela tela){
        this.context = context;
        this.tela = tela;
    }

    public void obterTimeline(){
        try{
            //Obtem url
            String sUrl = Settings.URL + "message/timeline";

            //Obtem usuario logado
            LoginDAO oLoginDAO = new LoginDAO(context);
            Login oLogin = oLoginDAO.getLogin();

            //Prepara JSON
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("userid", oLogin.getUserID());
            jsonParams.put("token", oLogin.getToken());

            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (Request.Method.POST, sUrl, jsonParams, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response){
                            try {
                                String sReturn = response.getString("return");
                                if(!sReturn.equalsIgnoreCase("NOTFOUND") && !sReturn.equalsIgnoreCase("ERROR") && !sReturn.equalsIgnoreCase("NOT_LOGGED") ){
                                    JSONArray sArray = response.getJSONArray("return");
                                    List<Message> listMessage = new ArrayList<Message>();
                                    for(int n = 0; n < sArray.length(); n++) {
                                        JSONObject object = sArray.getJSONObject(n);

                                        //Instancia objeto mensagem
                                        Message oMsg = new Message();
                                        oMsg.setDatetime(object.getString("datetime"));
                                        oMsg.setFavcount(object.getInt("favcount"));
                                        oMsg.setRtcount(object.getInt("rtcount"));
                                        oMsg.setId(object.getInt("id"));
                                        oMsg.setText(object.getString("text"));
                                        oMsg.setUser(object.getString("user"));

                                        String sImage = object.getString("image");
                                        if(sImage.equalsIgnoreCase("null")){
                                            sImage = "";
                                        }
                                        oMsg.setImage(sImage);

                                        String sAvatar = object.getString("avatar");
                                        if(sAvatar.equalsIgnoreCase("null")){
                                            sAvatar = "";
                                        }
                                        oMsg.setAvatar(sAvatar);

                                        String sLoc = object.getString("location");
                                        if(sLoc.equalsIgnoreCase("null")){
                                            oMsg.setLocation(null);
                                        } else {
                                            JSONObject oJSOLoc = object.getJSONObject("location");
                                            Location oLoc = new Location();
                                            oLoc.setLat(oJSOLoc.getString("lat"));
                                            oLoc.setLng(oJSOLoc.getString("lng"));
                                            oMsg.setLocation(oLoc);
                                        }

                                        listMessage.add(oMsg);
                                    }
                                    //Aciona a tela, para listar as mensagens
                                    tela.populateTimeline(listMessage);
                                } else {
                                    String sError = "";
                                    if(sReturn.equalsIgnoreCase("NOTFOUND")){
                                        sError = context.getResources().getString(R.string.notfound);
                                        Toast.makeText(context, sError, Toast.LENGTH_SHORT).show();
                                    }

                                    if(sReturn.equalsIgnoreCase("ERROR")){
                                        sError = context.getResources().getString(R.string.error);
                                        Toast.makeText(context, sError, Toast.LENGTH_SHORT).show();
                                    }

                                    if(sReturn.equalsIgnoreCase("NOT_LOGGED")){
                                        sError = context.getResources().getString(R.string.not_logged);
                                        Toast.makeText(context, sError, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            try {
                                throw new Exception(error.toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

            RequestQueue queue = Volley.newRequestQueue(context);
            queue.add(jsObjRequest);

        } catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
