package com.example.urubu.urubu;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.example.urubu.urubu.webservice.Settings;
import com.example.urubu.urubu.webservice.SettingsTela;
import com.example.urubu.urubu.webservice.SettingsWebDAO;
import com.example.urubu.urubu.webservice.User;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SettingsActivity extends AppCompatActivity implements SettingsTela {

    User oUser;
    static final int REQUEST_TAKE_PHOTO = 1;
    static final int REQUEST_EDIT_PHOTO = 2;
    static final int REQUEST_PHOTO_GALLERY = 3;
    File fImgAvatar;
    String mCurrentPhotoPath;
    ProgressDialog lDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        //DEFINE EVENTO DE CLIQUE NA IMAGEM DO USUARIO
        final ImageView imgAvatar = (ImageView) findViewById(R.id.imgAvatar);
        registerForContextMenu(imgAvatar);
        imgAvatar.setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View v) {
                //To register the button with context menu.
                registerForContextMenu(imgAvatar);
                openContextMenu(imgAvatar);
            }
        });

        lDialog = ProgressDialog.show(SettingsActivity.this, "", "Loading. Please wait...", true);
        SettingsWebDAO oDao = new SettingsWebDAO(getApplicationContext(), this);
        oDao.getSettings();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        String menuName = getResources().getString(R.string.img_ctx_menu);
        menu.setHeaderTitle(menuName);
        menu.add(0, 1, 0, "Tirar Foto");
        menu.add(0, 2, 0, "Escolher do álbum");

        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1: {
                takePictureIntent();
            }
            break;
            case 2: {
                getPictureFromGallery();
            }
            break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void setData(User PoUser) {
        oUser = PoUser;

        EditText edtName = (EditText)findViewById(R.id.set_name);
        edtName.setText(oUser.get_name());

        EditText edtLastName = (EditText)findViewById(R.id.set_lastname);
        edtLastName.setText(oUser.get_lastName());

        //Verifica se existe imagem no post, faz o download e apresenta na timeline
        if(oUser.get_avatar().length() > 0){
            ImageView oImgView = (ImageView)findViewById(R.id.imgAvatar);

            String sImgMsg = Settings.URL_uploads + oUser.get_avatar();
            ImageLoader imgLoad = LoadImage.getInstance(getApplicationContext()).getImageLoader();
            imgLoad.get(sImgMsg, imgLoad.getImageListener(oImgView,
                            R.drawable.ic_account_circle_black_48dp,
                            R.drawable.ic_account_circle_black_48dp),
                    600,
                    600
            );
        }

        lDialog.hide();
    }

    public void saveData(View v){
        EditText edtName = (EditText)findViewById(R.id.set_name);
        EditText edtLastName = (EditText)findViewById(R.id.set_lastname);

        boolean bValid = true;

        if(edtName.getText().length() == 0 && bValid){
            bValid = false;
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.name_error), Toast.LENGTH_SHORT).show();
        }

        if(edtLastName.getText().length() == 0 && bValid){
            bValid = false;
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.lastname_error), Toast.LENGTH_SHORT).show();
        }

        if(bValid){
            lDialog.show();

            oUser.set_name(edtName.getText().toString());
            oUser.set_lastName(edtLastName.getText().toString());

            String sImgFileData = null;
            String sImgFileName = null;
            if(fImgAvatar != null && fImgAvatar.getName() != oUser.get_avatar()){
                sImgFileData = getBase64(fImgAvatar.getPath());
                sImgFileName = fImgAvatar.getName();
            }

            SettingsWebDAO oDao = new SettingsWebDAO(getApplicationContext(), this);
            oDao.setSettings(oUser, sImgFileName, sImgFileData);
        }
    }

    @Override
    public void setSaved(){
        lDialog.hide();
        Toast.makeText(getApplicationContext(), "Salvo com sucesso!", Toast.LENGTH_SHORT).show();
        finish();
    }

    public void takePictureIntent() {
        try{
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            // Ensure that there's a camera activity to handle the intent
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                // Create the File where the photo should go
                fImgAvatar = null;
                try {
                    fImgAvatar = createImageFile();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                // Continue only if the File was successfully created
                if (fImgAvatar != null) {
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fImgAvatar));
                    startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                }
            }
        } catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public void getPictureFromGallery(){
        try{
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, REQUEST_PHOTO_GALLERY);
        } catch(Exception ex){
            ex.printStackTrace();
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";

        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES + "UrubuApp/");

        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_TAKE_PHOTO: {
                    Intent editIntent = new Intent(Intent.ACTION_EDIT);
                    editIntent.setDataAndType(Uri.fromFile(fImgAvatar), "image/*");
                    editIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivityForResult(Intent.createChooser(editIntent, null), REQUEST_EDIT_PHOTO);
                }
                break;
                case REQUEST_EDIT_PHOTO: {
                    Uri selectedImageURI = data.getData();
                    fImgAvatar = new File(getRealPathFromURI(selectedImageURI));

                    ImageView oImgAvatar = (ImageView) findViewById(R.id.imgAvatar);
                    oImgAvatar.setImageURI(data.getData());
                    oImgAvatar.setScaleType(ImageView.ScaleType.FIT_CENTER);
                }
                break;
                case REQUEST_PHOTO_GALLERY: {
                    Uri selectedImageURI = data.getData();
                    fImgAvatar = new File(getRealPathFromURI(selectedImageURI));

                    ImageView oImgAvatar = (ImageView) findViewById(R.id.imgAvatar);
                    oImgAvatar.setImageURI(data.getData());
                    oImgAvatar.setScaleType(ImageView.ScaleType.FIT_CENTER);
                }
                break;
            }
        } else if (resultCode == RESULT_CANCELED) {
            // Use the Builder class for convenient dialog construction
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.camera_cancelled)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) { }
                    });
            // Create the AlertDialog object and return it
            AlertDialog oDialog = builder.create();
            oDialog.show();
        } else {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.camera_fail)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) { }
                    });
            // Create the AlertDialog object and return it
            AlertDialog oDialog = builder.create();
            oDialog.show();
        }
    }

    public String getBase64(String PsFilePath){
        Bitmap bm = BitmapFactory.decodeFile(PsFilePath);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 10, baos);

        byte[] b = baos.toByteArray();
        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encodedImage;
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

}

