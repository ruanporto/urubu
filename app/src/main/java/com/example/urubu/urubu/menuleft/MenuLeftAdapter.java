package com.example.urubu.urubu.menuleft;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.urubu.urubu.R;

import java.util.List;

/**
 * Created by Ruan on 27/10/2015.
 */
public class MenuLeftAdapter extends BaseAdapter {

    List<String> itensList;

    public MenuLeftAdapter(List<String> itensList){
        this.itensList = itensList;
    }


    @Override
    public int getCount() {
        return itensList.size();
    }

    @Override
    public Object getItem(int i) {
        return itensList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {

            LayoutInflater inflater = (LayoutInflater) viewGroup.getContext().getSystemService(viewGroup.getContext().LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.left_menu_list_itens,null);

            TextView txtView = (TextView) view.findViewById(R.id.id_txtitem_leftmenu);
            ImageView imgView = (ImageView) view.findViewById(R.id.img_item_leftmenu);

            //Set list item text
            txtView.setText(itensList.get(i));

            //Set item list ICON
            switch (i) {
                case 0:
                    imgView.setImageResource(R.drawable.house32); //HOME
                    break;
                case 1:
                    imgView.setImageResource(R.drawable.fire32); //TRENDS
                    break;
                case 2:
                    imgView.setImageResource(R.drawable.gear32); //CONFIGURATION
                    break;
                case 3:
                    imgView.setImageResource(R.drawable.logout32); //CONFIGURATION
                    break;
            }
        }

        return view;
    }
}
