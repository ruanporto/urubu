package com.example.urubu.urubu.telasconteudo.telasexternas;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.example.urubu.urubu.LoadImage;
import com.example.urubu.urubu.R;
import com.example.urubu.urubu.webservice.Location;
import com.example.urubu.urubu.webservice.Message;
import com.example.urubu.urubu.webservice.Settings;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Ruan on 31/10/2015.
 */
public class UserMessagesAdapter  extends BaseAdapter {

    public List<Message> listMessages;
    Context context;

    public UserMessagesAdapter(Context context, List<Message> listMessages) {
        this.listMessages = listMessages;
        this.context = context;
    }

    @Override
    public int getCount() {
        return listMessages.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(viewGroup.getContext().LAYOUT_INFLATER_SERVICE);

                view = inflater.inflate(R.layout.list_messages, null);

                //Objetos para ajuste de data
                SimpleDateFormat oDtFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                SimpleDateFormat oDtShowF = new SimpleDateFormat("dd/MM/yyyy");

                //Objeto mensagem
                Message msg = listMessages.get(i);

                //Componentes de tela
                ImageView profileImage = (ImageView) view.findViewById(R.id.imgProfile);
                TextView txtMessage = (TextView) view.findViewById(R.id.txtMessage);
                TextView txtUserName = (TextView) view.findViewById(R.id.txtUserName);
                TextView txtDate = (TextView) view.findViewById(R.id.txtMsgDt);
                ImageView imageMsg = (ImageView) view.findViewById(R.id.imgMessage);
                LinearLayout layoutLoc = (LinearLayout) view.findViewById(R.id.layoutLoc);
                TextView txtLoc = (TextView) view.findViewById(R.id.txtLoc);

                //componente para destacar/clicar em hashtags
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View textView) {
                        Toast.makeText(textView.getContext(), "Click", Toast.LENGTH_LONG).show();
                    }
                };
                SpannableString ssMsg = new SpannableString(msg.getText());
                Matcher matcher = Pattern.compile("[#]+[A-Za-z0-9-_]+\\b").matcher(msg.getText());
                while (matcher.find()) {
                    ssMsg.setSpan(clickableSpan, matcher.start(), matcher.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                //Adiciona texto da mensagem
                txtMessage.setText(ssMsg, TextView.BufferType.SPANNABLE);
                txtMessage.setMovementMethod(LinkMovementMethod.getInstance());
                txtMessage.setClickable(false);

                //Adiciona nome do usuário
                txtUserName.setText(msg.getUser());

                //Adiciona data da mensagem
                Date oDateMsg = null;
                try {
                    oDateMsg = oDtFormat.parse(msg.getDatetime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                txtDate.setText(oDtShowF.format(oDateMsg));

                //Verifica se existe imagem no post, faz o download e apresenta na timeline
                if (msg.getImage().length() > 0) {
                    String sImgMsg = Settings.URL_uploads + msg.getImage();
                    ImageLoader imgLoad = LoadImage.getInstance(view.getContext()).getImageLoader();
                    imgLoad.get(sImgMsg, imgLoad.getImageListener(imageMsg,
                                    R.drawable.ic_photo_size_select_actual_black_24dp,
                                    R.drawable.ic_photo_size_select_actual_black_24dp),
                            600,
                            600
                    );
                    imageMsg.setVisibility(View.VISIBLE);
                    imageMsg.setClickable(false);
                }

                //Verifica se existe imagem no post, faz o download e apresenta na timeline
                if (msg.getAvatar().length() > 0) {

                    String sImgAva = Settings.URL_uploads + msg.getAvatar();
                    ImageLoader avaLoad = LoadImage.getInstance(view.getContext()).getImageLoader();
                    avaLoad.get(sImgAva, avaLoad.getImageListener(profileImage,
                                    R.drawable.ic_account_circle_black_48dp,
                                    R.drawable.ic_account_circle_black_48dp),
                            600,
                            600
                    );
                    profileImage.setClickable(false);
                }

                if (msg.getLocation() != null) {
                    Location oLoc = msg.getLocation();
                    String sLatLng = oLoc.getLat() + "," + oLoc.getLng();

                    //componente para abrir mapa
                    ClickableSpan clickMap = new ClickableSpan() {
                        @Override
                        public void onClick(View textView) {
                            //Get contoller
                            TextView tv = (TextView) textView;
                            // Create a Uri from an intent string. Use the result to create an Intent.
                            Uri gmmIntentUri = Uri.parse("geo:" + tv.getText().toString());
                            // Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                            // Make the Intent explicit by setting the Google Maps package
                            mapIntent.setPackage("com.google.android.apps.maps");
                            // Attempt to start an activity that can handle the Intent
                            textView.getContext().startActivity(mapIntent);
                        }
                    };

                    SpannableString ssMap = new SpannableString(sLatLng);
                    ssMap.setSpan(clickMap, 0, sLatLng.length(), 0);
                    txtLoc.setText(ssMap);

                    txtLoc.setMovementMethod(LinkMovementMethod.getInstance());
                    layoutLoc.setVisibility(View.VISIBLE);
                }
            }

        return view;
    }
}
