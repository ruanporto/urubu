package com.example.urubu.urubu.webservice;

/**
 * Created by Norton on 02/10/2015.
 */
public class Settings {
    public static String URL = "http://urubu.jossandro.com/api/";
    public static String URL_uploads = URL + "uploads/";
    public static String URL_usersSearch = URL + "users/";
    public static String URL_userMessages = URL + "message/"; //mostra as mensagens da timeline de um usuario pesquisado
    public static String URL_hashtagMessages = URL + "message/hashtag/";
}
