package com.example.urubu.urubu.webservice;

import java.util.Date;

/**
 * Created by Norton on 02/10/2015.
 */
public class User {

    private String _id;
    private String _name;
    private String _lastName;
    private String _userName;
    private String _token;
    private Date _tokenVal;
    private String _about;
    private String _email;
    private String _avatar;
    private String _pass;

    //Constructor method
    public User(String _about, String _avatar, String _email, String _id, String _lastName, String _name, String _token, Date _tokenVal, String _userName) {
        this._about = _about;
        this._avatar = _avatar;
        this._email = _email;
        this._id = _id;
        this._lastName = _lastName;
        this._name = _name;
        this._token = _token;
        this._tokenVal = _tokenVal;
        this._userName = _userName;
    }

    //Constructor method
    public User(){}

    //Getters and Setters
    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_lastName() {
        return _lastName;
    }

    public void set_lastName(String _lastName) {
        this._lastName = _lastName;
    }

    public String get_userName() {
        return _userName;
    }

    public void set_userName(String _userName) {
        this._userName = _userName;
    }

    public String get_token() {
        return _token;
    }

    public void set_token(String _token) {
        this._token = _token;
    }

    public Date get_tokenVal() {
        return _tokenVal;
    }

    public void set_tokenVal(Date _tokenVal) {
        this._tokenVal = _tokenVal;
    }

    public String get_about() {
        return _about;
    }

    public void set_about(String _about) {
        this._about = _about;
    }

    public String get_email() {
        return _email;
    }

    public void set_email(String _email) {
        this._email = _email;
    }

    public String get_avatar() {
        return _avatar;
    }

    public void set_avatar(String _avatar) {
        this._avatar = _avatar;
    }

    public String get_pass() {
        return _pass;
    }

    public void set_pass(String _pass) {
        this._pass = _pass;
    }
}
