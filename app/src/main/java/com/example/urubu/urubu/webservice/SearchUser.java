package com.example.urubu.urubu.webservice;

import java.util.List;

/**
 * Created by Ruan on 05/11/2015.
 */
public interface SearchUser {
    public void populateUsersListOnScreen(List<User> userList);
    public void populateHashtagMessagesOnScreen(List<Message> messagesList);
    public void createUserHeader(List<User> user);
    public void changeFollowButton();
}
