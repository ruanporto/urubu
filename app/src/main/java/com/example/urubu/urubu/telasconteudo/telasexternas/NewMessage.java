package com.example.urubu.urubu.telasconteudo.telasexternas;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.example.urubu.urubu.CreateUserActivity;
import com.example.urubu.urubu.R;
import com.example.urubu.urubu.webservice.MessageDAO;

public class NewMessage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_message);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_message, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    public void NewMessage_click(View v)
    {
        UserMessages oUserMessages = new UserMessages();
        MessageDAO oMessage = new MessageDAO(oUserMessages);
        EditText edTextMessage = (EditText)findViewById(R.id.txtNewMessage);
        String sTextMessage = edTextMessage.getText().toString();
        Intent intent = new Intent(this, CreateUserActivity.class);

        oMessage.NewMessage(sTextMessage,this.getApplicationContext());
    }

    public void uploadImgToMessage(View v){
        //TODO-RUAN
    }
}
