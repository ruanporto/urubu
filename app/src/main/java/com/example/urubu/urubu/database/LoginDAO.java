package com.example.urubu.urubu.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.File;

/**
 * Created by Norton on 04/10/2015.
 */
public class LoginDAO {
    Context context;
    private LoginDbHelper mdb;
    private SQLiteDatabase database;
    private String[] colunas = {
        LoginContract.Login.USERNAME,
        LoginContract.Login.TOKEN,
        LoginContract.Login.TOKENVAL,
        LoginContract.Login.USERID,
    };

    public LoginDAO (Context context){
        this.mdb = new LoginDbHelper(context);
        this.context = context;
    }

    public void open() throws SQLException {
        database = mdb.getWritableDatabase();
    }

    public void close() {
        mdb.close();
    }

    public Login insertToken(Login l){
        ContentValues dadosToken = new ContentValues();
        dadosToken.put(LoginContract.Login.USERNAME, l.getUserName());
        dadosToken.put(LoginContract.Login.TOKEN, l.getToken());
        dadosToken.put(LoginContract.Login.TOKENVAL, l.getTokenVal());
        dadosToken.put(LoginContract.Login.USERID, l.getUserID());

        try {
            this.open();
            long id = database.insert(
                    LoginContract.Login.TABLE_NAME,
                    null,   //The second argument provides the name of a column in which the framework can insert NULL in the event that the ContentValues is empty (if you instead set this to "null", then the framework will not insert a row when there are no values).
                    dadosToken);

            if(id == -1)
                throw new Exception("Não foi possível gravar o login");

            return l;
        }catch (SQLException ex){
            ex.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            this.close();
        }
    }

    public void deletePessoa(Login l) {
        try {
            this.open(); //open connection
            String token = l.getToken();
            Log.d("URUBU", "Pessoa deleted with id: " + l.getUserID());
            database.delete(LoginContract.Login.TABLE_NAME, LoginContract.Login.TOKEN + " = '" + token + "'", null);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            this.close();
        }
    }

    public Login getLogin() {
        try {
            this.open();
            Cursor c = database.query(
                    LoginContract.Login.TABLE_NAME,      // The table to query
                    colunas,                               // The columns to return
                    null,                                // The columns for the WHERE clause
                    null,                            // The values for the WHERE clause
                    null,                                     // don't group the rows
                    null,                                     // don't filter by row groups
                    null                                 // The sort order
            );

            if(c.getCount() > 0){
                c.moveToFirst();
                Login l = cursorToLogin(c);
                return l;
            }else
                return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            this.close();
        }
    }

    public Boolean databaseExists(){
        File database = this.context.getDatabasePath("LOGIN.db");
        return database.exists();
    }

    private Login cursorToLogin(Cursor cursor) {
        Login login = new Login();
        login.setUserName(cursor.getString(0));
        login.setToken(cursor.getString(1));
        login.setTokenVal(cursor.getString(2));
        login.setUserID(cursor.getString(3));
        return login;
    }


}
