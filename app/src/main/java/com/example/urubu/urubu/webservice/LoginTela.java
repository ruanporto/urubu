package com.example.urubu.urubu.webservice;

import android.view.View;

import java.util.Date;

/**
 * Created by Norton on 04/10/2015.
 */
public interface LoginTela {

    public void login(View v);
    public void efetuaLogin(String PsToken, String PsTokenVal, String UserName, String PsUserID);

}
