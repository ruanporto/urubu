package com.example.urubu.urubu.webservice;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.urubu.urubu.database.Login;
import com.example.urubu.urubu.database.LoginDAO;
import com.example.urubu.urubu.telasconteudo.telasexternas.UserMessages;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ruan on 31/10/2015.
 */
public class MessageDAO {

    public Context context;
    public UserMessages usrMsgsTela;
    public Intent intentX;

    public MessageDAO(UserMessages usrMsgsTela){
        this.context = (Context) usrMsgsTela;
        this.usrMsgsTela = usrMsgsTela;
    }

    //Get all the message from a user
    //<param name='username'>Name of the user you want to retrieve messages</param>
    public void getAllMessages(String username) {
        //public void getAllPessoas() {
        String url = Settings.URL_userMessages + username;
        List<Message> messages = new ArrayList<Message>();

        JsonObjectRequest req = new JsonObjectRequest(
                Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response){

                List<Message> messages = new ArrayList<Message>();

                //Get the ArrayJson from ObjectJson
                JSONArray jsonArray = null;
                try {
                    jsonArray = response.getJSONArray("return");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    for(int i =0; i < jsonArray.length(); i++) {
                        JSONObject jsonKeyValue = jsonArray.getJSONObject(i);
                        Message message = jsobjToMessage(jsonKeyValue);
                        messages.add(message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Log.d("WBS", pessoas.toString());
                usrMsgsTela.populateMsgsOnScreen(messages);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub
                Toast.makeText(context, "Problema ao buscar dados da web", Toast.LENGTH_SHORT).show();
                Log.d("WBS", error.toString());
            }
        }
        );

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(req);

    }

    //Tranform json object into a specific class obj based on the atribute's name declared on the API doc
    private Message jsobjToMessage(JSONObject json){
        Message msg = new Message();
        try{
            msg.setId(json.getInt("id"));
            msg.setText(json.getString("text"));
            msg.setUser(json.getString("user"));
            msg.setRtcount(json.getInt("rtcount"));
            msg.setFavcount(json.getInt("favcount"));
            msg.setDatetime(json.getString("datetime"));
            msg.setImage(json.getString("image").equalsIgnoreCase("null") ? "" : json.getString("image"));
            msg.setAvatar(json.getString("avatar").equalsIgnoreCase("null") ? "" : json.getString("avatar"));

            String sLoc = json.getString("location");
            if(sLoc.equalsIgnoreCase("null")){
                msg.setLocation(null);
            } else {
                JSONObject oJSOLoc = json.getJSONObject("location");
                Location oLoc = new Location();
                oLoc.setLat(oJSOLoc.getString("lat"));
                oLoc.setLng(oJSOLoc.getString("lng"));
                msg.setLocation(oLoc);
            }
        }
        catch (JSONException e){
            e.printStackTrace();
        }

        return msg;
    }

    public boolean NewMessage(String Message, final Context contextX)
    {
        this.intentX = intentX;
        try{
            String sUrl = Settings.URL + "message";
            //Obtem usuario logado
            LoginDAO oLoginDAO = new LoginDAO(contextX);
            Login oLogin = oLoginDAO.getLogin();
            Long tsLong = System.currentTimeMillis()/1000;
            String ts = tsLong.toString();
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("userid", oLogin.getUserID().toString());
            jsonParams.put("token", oLogin.getToken().toString());
            jsonParams.put("text", Message);
            jsonParams.put("datetime", ts);
            jsonParams.put("location",null);
            jsonParams.put("imageName", null);
            jsonParams.put("image", null);

            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (Request.Method.POST, sUrl, jsonParams, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response){
                            try {
                                String sReturn = response.getString("return");

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            try {

                                throw new Exception(error.toString());
                               } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

            RequestQueue queue = Volley.newRequestQueue(contextX);
            queue.add(jsObjRequest);
            return true;
        }catch (Exception ex){
            Log.e("UsersWebDAO_NewMessage", ex.getMessage());
            return false;
        }
    }
}
