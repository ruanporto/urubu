package com.example.urubu.urubu.webservice;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Base64;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.urubu.urubu.R;
import com.example.urubu.urubu.database.Login;
import com.example.urubu.urubu.database.LoginDAO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Norton on 07/11/2015.
 */
public class SettingsWebDAO {

    private Context context;
    private SettingsTela tela;

    public SettingsWebDAO(Context context, SettingsTela tela){
        this.context = context;
        this.tela = tela;
    }

    public void getSettings(){
        try{
            //Obtem usuario logado
            LoginDAO oLoginDAO = new LoginDAO(context);
            final Login oLogin = oLoginDAO.getLogin();

            //Obtem url
            String sUrl = Settings.URL + "users/" + oLogin.getUserName();

            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (sUrl, null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response){
                            try {
                                String sReturn = response.getString("return");
                                if(!sReturn.equalsIgnoreCase("NOTFOUND") && !sReturn.equalsIgnoreCase("ERROR")){
                                    JSONArray oJsArray = response.getJSONArray("return");

                                    JSONObject oJsObject = null;
                                    for(int i =0; i<oJsArray.length(); i++){
                                        JSONObject oJObject = oJsArray.getJSONObject(i);
                                        if(oJObject.getString("id").equalsIgnoreCase(oLogin.getUserID())){
                                            oJsObject = oJObject;
                                            break;
                                        }
                                    }

                                    User oUser = new User();
                                    oUser.set_id(oJsObject.getString("id"));
                                    oUser.set_name(oJsObject.getString("name"));
                                    oUser.set_lastName(oJsObject.getString("lastname"));
                                    oUser.set_token(null);
                                    oUser.set_tokenVal(null);
                                    oUser.set_about(oJsObject.getString("about"));
                                    oUser.set_email(oJsObject.getString("email"));
                                    oUser.set_avatar(oJsObject.getString("avatar"));

                                    tela.setData(oUser);
                                } else {
                                    String sError = "";
                                    if(sReturn.equalsIgnoreCase("NOTFOUND")){
                                        sError = context.getResources().getString(R.string.notfound);
                                        Toast.makeText(context, sError, Toast.LENGTH_SHORT).show();
                                    }

                                    if(sReturn.equalsIgnoreCase("ERROR")){
                                        sError = context.getResources().getString(R.string.error);
                                        Toast.makeText(context, sError, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            try {
                                throw new Exception(error.toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

            RequestQueue queue = Volley.newRequestQueue(context);
            queue.add(jsObjRequest);

        } catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public void setSettings(User PoUser, String PsImgName, String PsImgData){
        try{
            //Obtem usuario logado
            LoginDAO oLoginDAO = new LoginDAO(context);
            Login oLogin = oLoginDAO.getLogin();

            //Obtem url
            String sUrl = Settings.URL + "users/";

            //Prepara JSON
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("userid", oLogin.getUserID());
            jsonParams.put("token", oLogin.getToken());
            jsonParams.put("username", null);
            jsonParams.put("name", PoUser.get_name());
            jsonParams.put("lastname", PoUser.get_lastName());
            jsonParams.put("email", null);
            jsonParams.put("about", null);
            jsonParams.put("newpass", null);
            jsonParams.put("oldpass", null);
            jsonParams.put("avatarFile", PsImgData);
            jsonParams.put("avatarName", PsImgName);

            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (Request.Method.PUT, sUrl, jsonParams, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response){
                            try {
                                String sReturn = response.getString("return");
                                if(sReturn.equalsIgnoreCase("TRUE")){
                                    tela.setSaved();
                                } else {
                                    String sError = "";
                                    if(sReturn.equalsIgnoreCase("FILE_NOT_ALLOWED")){
                                        sError = context.getResources().getString(R.string.file_not_allowed);
                                        Toast.makeText(context, sError, Toast.LENGTH_SHORT).show();
                                    }

                                    if(sReturn.equalsIgnoreCase("FILE_TOO_BIG")){
                                        sError = context.getResources().getString(R.string.file_too_big);
                                        Toast.makeText(context, sError, Toast.LENGTH_SHORT).show();
                                    }

                                    if(sReturn.equalsIgnoreCase("ERROR")){
                                        sError = context.getResources().getString(R.string.error);
                                        Toast.makeText(context, sError, Toast.LENGTH_SHORT).show();
                                    }

                                    if(sReturn.equalsIgnoreCase("NOT_LOGGED")){
                                        sError = context.getResources().getString(R.string.not_logged);
                                        Toast.makeText(context, sError, Toast.LENGTH_SHORT).show();
                                    }

                                    if(sReturn.equalsIgnoreCase("INVALID_PASSWORD")){
                                        sError = context.getResources().getString(R.string.invalid_password);
                                        Toast.makeText(context, sError, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            try {
                                throw new Exception(error.toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

            RequestQueue queue = Volley.newRequestQueue(context);
            queue.add(jsObjRequest);

        } catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
