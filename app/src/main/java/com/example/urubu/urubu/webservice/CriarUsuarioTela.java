package com.example.urubu.urubu.webservice;

import android.view.View;

/**
 * Created by Norton on 31/10/2015.
 */
public interface CriarUsuarioTela {
    void validaLogin(View v);
    void finalizaTela();
}
