package com.example.urubu.urubu.webservice;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Norton on 31/10/2015.
 */
public class Message implements Serializable {
    private int id;
    private String text;
    private String image;
    private Location location;
    private String user;
    private int rtcount;
    private int favcount;
    private String datetime;
    private String avatar;

    public Message() {
        this.datetime = "";
        this.favcount = 0;
        this.id = 0;
        this.image = "";
        this.location = null;
        this.rtcount = 0;
        this.text = "";
        this.user = "";
        this.avatar = null;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public int getFavcount() {
        return favcount;
    }

    public void setFavcount(int favcount) {
        this.favcount = favcount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public int getRtcount() {
        return rtcount;
    }

    public void setRtcount(int rtcount) {
        this.rtcount = rtcount;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
